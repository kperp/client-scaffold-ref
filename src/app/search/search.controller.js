/**
 * @ngdoc controller
 * @name search.controller.js
 * @description SearchController controller
 */
(function() {
	"use strict";

	angular // eslint-disable-line no-undef
		.module("app")
		.controller("SearchController", SearchController);

	SearchController.$inject = ['$scope','SearchService'];

	/** @ngInject */
	function SearchController($scope, SearchService) {
		var vm = this;
		this.input = {};

		/***********************************************************************
		 * Local variables.
		 **********************************************************************/
		//var bar;

		/***********************************************************************
		 * Exported variables.
		 **********************************************************************/
		// $scope.test = "test";

		/***********************************************************************
		 * Exported functions.
		 **********************************************************************/
		//vm.foo = foo;

		vm.executeSearch = executeSearch;

		var client = elasticsearch.Client({
			host: '192.168.2.2:9200',
			log: 'trace'
		});

		// Activating the controller.
		activate();

		/***********************************************************************
		 * Controller activation.
		 **********************************************************************/
		function activate() {
		}

		/***********************************************************************
		 * $scope destroy.
		 **********************************************************************/
		// $scope.$on("$destroy", function() {
		// });

		/***********************************************************************
		 * Functions.
		 **********************************************************************/
		//function foo(bar) {
		//}

		// $scope.executeSearch = function() {
		// $scope.executeSearch = function() {		
		function executeSearch(event) {
			console.log(vm.input.searchparm);
		}
	}
})();
