angular.module('app')
.component('searchComponent', {
	templateUrl: 'app/search/search.html',
  	controller: 'SearchController',
  	controllerAs: 'vm'
});