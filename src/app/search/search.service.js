/**
 * @ngdoc service
 * @name search.factory.js
 * @description SearchService factory
 */
(function() {
	"use strict";

	angular // eslint-disable-line no-undef
		.module("app")
		.factory("SearchService", SearchService);

	/** @ngInject */
	function SearchService() {
		/***********************************************************************
		 * Local variables.
		 **********************************************************************/
		//var bar;

		/***********************************************************************
		 * Local functions.
		 **********************************************************************/
		//var foo = function() {
		//};

		/***********************************************************************
		 * Exported functions and variables.
		 **********************************************************************/
		//return {
		//	fooBar: function () {
		//	},
		//};

		return {
			factory : function(esFactory) {
				return esFactory({
					host: '192.168.2.2:9200'
				});
			}
		};
	}
})();
