# README #

simple angular scaffold reference based on fountainJS.
few angular files based on [John Papa's angular styleguide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)

### What is this repository for? ###

* to setup and try out things quicly
* 0.1
* also ElasticSearch JS client included as bower dependency 

### How do I get set up? ###

* npm install
* bower install
* gulp serve
* vagrant up jerry - vagrant ssh and change /etc/elasticsearch/elasticsearch.yml to bind at 0.0.0.0